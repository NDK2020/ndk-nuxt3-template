import { defineNuxtConfig } from "nuxt";
import { IntlifyModuleOptions } from "@intlify/nuxt3";
import Components from "unplugin-vue-components/vite";
import { AntDesignVueResolver } from "unplugin-vue-components/resolvers";

declare module "@nuxt/schema" {
  interface NuxtConfig {
    intlify?: IntlifyModuleOptions;
  }
}

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  app: {
    head: {
      titleTemplate: "ndkhoa - %s",
      htmlAttrs: {
        lang: "en",
      },
      meta: [
        { name: "viewport", content: "width=device-width, initial-scale=1" },
        {
          hid: "description",
          name: "description",
          content: "nuxt 3 ndk template",
        },
      ],
      link: [{ rel: "icon", type: "image/png", href: "/favicon.png" }],
    },
  },
  srcDir: "client/",
  components: {
    global: true,
    dirs: [
      "~/components/atoms",
      "~/components/molecules",
      "~/lib/components",
      "~/content",
      "~/components",
    ],
  },
  css: [
    // "ant-design-vue/dist/antd.css",
  ],

  buildModules: [
    "@unocss/nuxt",
  ],

  modules: [
    "@pinia/nuxt",
    "@intlify/nuxt3",
    "@vueuse/nuxt",
    "@nuxt/content",
  ],

  content: {
    highlight: {
      theme: "dracula-soft",
    },
  },

  intlify: {
    localeDir: "lib/project/locales",
    vueI18n: {
      locale: "en",
      fallbackLocale: "en",
      availableLocales: ["en", "vi"],
    },
  },
  /* experimental: {
    reactivityTransform: true,
    viteNode: true,
  }, */
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@import "@/assets/scss/index.scss";',
        },
      },
    },
    plugins: [
      // Components({
      //   resolvers: [AntDesignVueResolver({ resolveIcons: true })],
      // }),
    ],
    // @ts-expect-error: Missing ssr key
    ssr: {
      noExternal: ["moment", "compute-scroll-into-view" ],
    },
  },
});
