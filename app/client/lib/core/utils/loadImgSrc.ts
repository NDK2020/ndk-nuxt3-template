export const loadImgSrc = async (src: string) => {
  const fileType = src.match(/\.[0-9a-z]+$/i);
  const cleanPath = src.replace(fileType, "");
  const pathArray = cleanPath.split("/");
  const parentPath = pathArray[pathArray.length - 3];
  const folderPath = pathArray[pathArray.length - 2];
  const imagePath = pathArray[pathArray.length - 1];
  let data;
  switch (fileType[0]) {
    case ".jpg":
      data = (
        await import(
          /* @vite-ignore */ `../../../assets/images/${parentPath}/${folderPath}/${imagePath}.jpg`
        )
      ).default;
      break;
    case ".jpeg":
      data = (
        await import(
          /* @vite-ignore */ `../../../assets/images/${parentPath}/${folderPath}/${imagePath}.jpeg`
        )
      ).default;
      break;
    case ".png":
      data = (
        await import(
          /* @vite-ignore */ `../../../assets/images/${parentPath}/${folderPath}/${imagePath}.png`
        )
      ).default;
      break;
    case ".svg":
      data = (
        await import(
          /* @vite-ignore */ `../../../assets/images/${parentPath}/${folderPath}/${imagePath}.svg`
        )
      ).default;
      break;
    default:
      console.log(
        `Sorry, the image component can't recognize the ${fileType} file type just yet.`
      );
  }
  return data;
};
