import type { RouterConfig } from "@nuxt/schema";

// https://router.vuejs.org/api/#routeroptions
const headerHeight = 70;
const paddingTop = 40;
export default <RouterConfig>{
  scrollBehavior(to, _, savedPosition) {
    const getHeadingPos = () => {
      const heading = document.querySelector(to.hash) as any;
      return heading.offsetTop - headerHeight - paddingTop;
    };

    const nuxtApp = useNuxtApp();

    // If history back
    if (savedPosition) {
      // Handle Suspense resolution
      return new Promise((resolve) => {
        nuxtApp.hooks.hookOnce("page:finish", () => {
          setTimeout(() => resolve(savedPosition), 50);
        });
      });
    }
    // Scroll to heading on click
    if (to.hash) {
      //https://stackoverflow.com/questions/54535838/scroll-behaviour-vuejs-not-working-properly/67812331#67812331
      tryScrollToAnchor(to.hash, 7000, 100);
      async function tryScrollToAnchor(hash, timeout = 1000, delay = 100) {
        while (timeout > 0) {
          const el = document.querySelector(hash);
          if (el) {
            // el.scrollIntoView({ behavior: "smooth" });

            window.scrollTo({
              top: el.offsetTop - headerHeight - paddingTop,
              behavior: "smooth",
            });

            break;
          }
          await wait(delay);
          timeout = timeout - delay;
        }
      }
      function wait(duration) {
        return new Promise((resolve) => setTimeout(resolve, duration));
      }

      return;
    }

    // Scroll to top of window if click link from another page
    return new Promise((res, rej) => {
      setTimeout(() => {
        res({ top: 0 });
      }, 10);
    });
  },
};
