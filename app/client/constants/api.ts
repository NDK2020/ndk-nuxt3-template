export const EXAMPLE_API = process.env.NUXT_PUBLIC_EXAMPLE_API
export const AUTH_API = process.env.NUXT_PUBLIC_AUTH_API
export const FILE_API = process.env.NUXT_PUBLIC_FILE_API
export const USER_API = process.env.NUXT_PUBLIC_USER_API
